# Output of 
# In QGIS, you could use the Polygon from Layer Extent... tool from the toolbar (Vector > Research Tools > Polygon from Layer Extent...).
# https://gis.stackexchange.com/questions/176839/how-to-compute-max-and-min-coordinates-for-multiple-polygons

# wkt_geom	MINX	MINY	MAXX	MAXY	CNTX	CNTY	AREA	PERIM	HEIGHT	WIDTH
# Polygon ((172.65630506249010523 -38.0322885803083679, 172.65630506249010523 -34.39526331275412474, 175.55613012455972921 -34.39526331275412474, 175.55613012455972921 -38.0322885803083679, 172.65630506249010523 -38.0322885803083679))	172.65630506249	-38.0322885803084	175.55613012456	-34.3952633127541	174.106217593525	-36.2137759465312	10.5467370222343	13.0737006592477	3.63702526755424	2.89982506206962

upper_left = (172.65630506249010523, -34.39526331275412474)
lower_right = (175.55613012455972921, -38.0322885803083679)

digits = 3
rounding = '%.{}f'.format(digits)
print("Rounding to 3 digits")

def round_both(lat_lon_tuple):
	return ( float(rounding%lat_lon_tuple[0]), float(rounding%lat_lon_tuple[1]) )

upper_left_rounded = round_both(upper_left)
lower_right_rounded = round_both(lower_right)
print("upper_left: ", upper_left)
print("upper_left_rounded: ", upper_left_rounded)
print("lower_right:", lower_right)
print("lower_right_rounded:", lower_right_rounded)

# Determine extents
lon_extent = max(upper_left_rounded[0], lower_right_rounded[0]) - min(upper_left_rounded[0], lower_right_rounded[0])
lat_extent = max(upper_left_rounded[1], lower_right_rounded[1]) - min(upper_left_rounded[1], lower_right_rounded[1])
extent = round_both( (lon_extent,lat_extent) )
print("extent:", extent)

steps = tuple([int(ext*10**digits)+1 for ext in extent])
print("steps:", steps)
print("points:", "{:,}".format(steps[0]*steps[1	]))

on_lat_lon = upper_left_rounded
count = 0
with open("output{}.csv".format(digits), "w") as f:
	for lon_step in range(steps[0]):
		on_lat_lon = (upper_left_rounded[0] + lon_step * 1/10**digits, upper_left_rounded[1])
		for lat_step in range(steps[1]):
			on_lat_lon = round_both((on_lat_lon[0], upper_left_rounded[1] - lat_step * 1/10**digits))
			f.write("{},{}\n".format(on_lat_lon[0], on_lat_lon[1]))
			count += 1
			if count % 1000000 == 0:
				print(count)

# Rounding to 3 digits
# upper_left:  (172.6563050624901, -34.395263312754125)
# upper_left_rounded:  (172.656, -34.395)
# lower_right: (175.55613012455973, -38.03228858030837)
# lower_right_rounded: (175.556, -38.032)
# extent: (2.9, 3.637)
# steps: (2901, 3638)
# points: 10,553,838
